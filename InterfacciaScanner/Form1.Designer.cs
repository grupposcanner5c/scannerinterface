﻿namespace InterfacciaScanner
{
    partial class Form1
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label_datetime = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.label_time = new System.Windows.Forms.Label();
            this.pictureBox_uk = new System.Windows.Forms.PictureBox();
            this.pictureBox_italy = new System.Windows.Forms.PictureBox();
            this.btnConnection = new System.Windows.Forms.Button();
            this.label_welcome = new System.Windows.Forms.Label();
            this.label_user_name = new System.Windows.Forms.Label();
            this.label_entrance = new System.Windows.Forms.Label();
            this.label_entrance_left = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_uk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_italy)).BeginInit();
            this.SuspendLayout();
            // 
            // label_datetime
            // 
            this.label_datetime.AutoSize = true;
            this.label_datetime.Font = new System.Drawing.Font("Microsoft Sans Serif", 32.32F);
            this.label_datetime.Location = new System.Drawing.Point(8, 41);
            this.label_datetime.Name = "label_datetime";
            this.label_datetime.Size = new System.Drawing.Size(357, 52);
            this.label_datetime.TabIndex = 0;
            this.label_datetime.Text = "**/**/****    **:**:**";
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // label_time
            // 
            this.label_time.AutoSize = true;
            this.label_time.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.8F);
            this.label_time.Location = new System.Drawing.Point(12, 12);
            this.label_time.Name = "label_time";
            this.label_time.Size = new System.Drawing.Size(125, 29);
            this.label_time.TabIndex = 1;
            this.label_time.Text = "label_time";
            // 
            // pictureBox_uk
            // 
            this.pictureBox_uk.Image = global::InterfacciaScanner.Properties.Resources.uk1;
            this.pictureBox_uk.Location = new System.Drawing.Point(958, 12);
            this.pictureBox_uk.Name = "pictureBox_uk";
            this.pictureBox_uk.Size = new System.Drawing.Size(128, 128);
            this.pictureBox_uk.TabIndex = 3;
            this.pictureBox_uk.TabStop = false;
            this.pictureBox_uk.Click += new System.EventHandler(this.pictureBox_uk_Click);
            // 
            // pictureBox_italy
            // 
            this.pictureBox_italy.Image = global::InterfacciaScanner.Properties.Resources.italy1;
            this.pictureBox_italy.Location = new System.Drawing.Point(1124, 12);
            this.pictureBox_italy.Name = "pictureBox_italy";
            this.pictureBox_italy.Size = new System.Drawing.Size(128, 128);
            this.pictureBox_italy.TabIndex = 2;
            this.pictureBox_italy.TabStop = false;
            this.pictureBox_italy.Click += new System.EventHandler(this.pictureBox_italy_Click);
            // 
            // btnConnection
            // 
            this.btnConnection.Location = new System.Drawing.Point(571, 534);
            this.btnConnection.Name = "btnConnection";
            this.btnConnection.Size = new System.Drawing.Size(75, 23);
            this.btnConnection.TabIndex = 4;
            this.btnConnection.Text = "Connetti";
            this.btnConnection.UseVisualStyleBackColor = true;
            this.btnConnection.Click += new System.EventHandler(this.btnConnection_Click);
            // 
            // label_welcome
            // 
            this.label_welcome.AutoSize = true;
            this.label_welcome.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_welcome.Location = new System.Drawing.Point(375, 256);
            this.label_welcome.Name = "label_welcome";
            this.label_welcome.Size = new System.Drawing.Size(194, 42);
            this.label_welcome.TabIndex = 5;
            this.label_welcome.Text = "benvenuto";
            // 
            // label_user_name
            // 
            this.label_user_name.AutoSize = true;
            this.label_user_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_user_name.Location = new System.Drawing.Point(638, 256);
            this.label_user_name.Name = "label_user_name";
            this.label_user_name.Size = new System.Drawing.Size(102, 42);
            this.label_user_name.TabIndex = 6;
            this.label_user_name.Text = "******";
            // 
            // label_entrance
            // 
            this.label_entrance.AutoSize = true;
            this.label_entrance.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.8F);
            this.label_entrance.Location = new System.Drawing.Point(485, 313);
            this.label_entrance.Name = "label_entrance";
            this.label_entrance.Size = new System.Drawing.Size(184, 29);
            this.label_entrance.TabIndex = 7;
            this.label_entrance.Text = "entrate rimaste :";
            // 
            // label_entrance_left
            // 
            this.label_entrance_left.AutoSize = true;
            this.label_entrance_left.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.8F);
            this.label_entrance_left.Location = new System.Drawing.Point(675, 313);
            this.label_entrance_left.Name = "label_entrance_left";
            this.label_entrance_left.Size = new System.Drawing.Size(33, 29);
            this.label_entrance_left.TabIndex = 8;
            this.label_entrance_left.Text = "**";
            // 
            // Form1
            // 
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1264, 681);
            this.Controls.Add(this.label_entrance_left);
            this.Controls.Add(this.label_entrance);
            this.Controls.Add(this.label_user_name);
            this.Controls.Add(this.label_welcome);
            this.Controls.Add(this.btnConnection);
            this.Controls.Add(this.pictureBox_uk);
            this.Controls.Add(this.pictureBox_italy);
            this.Controls.Add(this.label_time);
            this.Controls.Add(this.label_datetime);
            this.Name = "Form1";
            this.Text = "InterfacciaScanner";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_uk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_italy)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_datetime;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label label_time;
        private System.Windows.Forms.PictureBox pictureBox_italy;
        private System.Windows.Forms.PictureBox pictureBox_uk;
        private System.Windows.Forms.Button btnConnection;
        private System.Windows.Forms.Label label_welcome;
        private System.Windows.Forms.Label label_user_name;
        private System.Windows.Forms.Label label_entrance;
        private System.Windows.Forms.Label label_entrance_left;
    }
}

